#include<stdio.h>
int main(){
	int m1[3][3] , m2[3][3] , a[3][3]={0} , m[3][3]={0};
	int i,j,k,r1,c1,r2,c2;

	printf("Enter number of ROWS and COLOUMNS in matrix 1:\n");
	scanf("%d%d",&r1,&c1);

	printf("Enter number of ROWS and COLOMNS in matrix 2:\n");
	scanf("%d%d",&r2,&c2);

	if (r1 != r2 || c1 != c2)
	{
		printf("Matrix cannot be added.");
		return 0;
	}
	else if (c1 != r2)
	{
		printf("Matrix cannot be added.");
	}
	else 
	{
		printf("Enter elements of matrix 1:\n");
		for (i=0 ; i<r1 ; i++)
			for (j=0 ; j<c1 ; j++)
				scanf("%d",&m1[i][j]);

		printf("Enter elements of matrix 2:\n");
		for (i=0 ; i<r2 ; i++)
			for (j=0 ; j<c2 ; j++)
				scanf("%d",&m2[i][j]);

		//ADDITION
		for (i=0 ; i<r1 ; i++)
			for(j=0 ; j<c1 ; j++)
				a[i][j] = m1[i][j] + m2[i][j];

		printf("\n Addition of matrix\n");
		for (i=0 ; i<r1 ; i++)
		{
			for (j=0 ; j<c1 ; j++)
				printf("%d",a[i][j]);
			printf("\n");
		}

		//MULTIPLICATION
		for(i=0 ; i<r1 ; i++)
			for (j=0 ; j<c2 ; j++)
				for (k=0 ; k<r2 ; k++)
					m[i][j] += m1[i][k] * m2[k][j];

		printf("\n Multiplication of matrix:\n");
		for (i=0 ;i<r1 ; i++)
		{
			for (j=0 ; j<c2 ; j++)
				printf("%d",m[i][j]);
			printf("\n");
		}
	}
	return 0;
}
